# *LAPORAN RESMI FINAL PROJECT PRAKTIKUM SISTEM OPERASI*
Berikut adalah Laporan Resmi Final Project Praktikum Sistem Operasi oleh Kelompok IT 09.

---
## *TABLE OF CONTENT*

 - [Soal A](#soala)
 - [Soal B](#soalb)
 - [Soal C](#soalc)
 - [Soal D](#soald)
 - [Soal E](#soale)
 - [Soal F](#soalf)
 - [Soal G](#soalg)
 - [Soal H](#soalh)
 - [cron.sh](#cronsh)
 
---

### **A. SOAL A<a name="soala"></a>**
**- AUTENTIKASI**

1. Auntetikasi dilakukan melalui fungsi `_auth_user`, yang menerima permintaan yang berisi nama pengguna dan kata sandi. Pengguna root diizinkan mengakses tanpa pemeriksaan lebih lanjut. Untuk pengguna lain, fungsi ini akan memeriksa apakah nama pengguna dan kata sandi yang diberikan sesuai dengan entri dalam file pengguna.

2. Kode ini mengimplementasikan fungsi `_auth_user` untuk melakukan otentikasi pengguna. Pertama, kode tersebut melakukan parsing terhadap string permintaan (request) untuk mendapatkan nama pengguna (`user_name`) dan kata sandi (`user_password`). Selanjutnya, kode mengecek apakah pengguna yang melakukan otentikasi adalah "root". Jika ya, maka otentikasi dianggap berhasil, dan respons "AUTH USER OK" dikirim. Jika bukan "root", maka dilakukan pengecekan terhadap basis data pengguna untuk memastikan bahwa nama pengguna dan kata sandi yang diberikan valid. Pengecekan ini dilakukan dengan menggunakan command-line tool 'grep'. Jika nama pengguna dan kata sandi tidak cocok atau pengguna tidak terdaftar, respons akan mengindikasikan kesalahan dengan menyertakan pesan yang sesuai. Jika otentikasi berhasil, respons "AUTH USER OK" dikirim. Keseluruhan fungsi ini dirancang untuk memastikan bahwa pengguna yang mencoba mengakses basis data memiliki izin yang sesuai dengan autentikasi yang benar.

```c
int _auth_user(char *request, char *response) {
    // parsing request
    char user_name[BUFFER_SIZE];
    char user_password[BUFFER_SIZE];

    // ...

    // check user
    if (strcmp(user_name, "root") == 0) {
        sprintf(response, "AUTH USER OK\n");
        return EXIT_SUCCESS;
    }

    char* check_user = format_string(
        "grep -wq '%s,%s' '%s'",
        user_name, user_password, users_path
    );

    if (system(check_user) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User is not registered or wrong password"
        );
        return EXIT_FAILURE;
    }
    free(check_user);

    sprintf(response, "AUTH USER OK\n");
    return EXIT_SUCCESS;
}

```

**- OUTPUT** 

![autenti](/uploads/4ab06aaf8cc94a96ca9fd1d194159367/autenti.png)

### **B. SOAL B<a name="soalb"></a>**

**- AUTORISASI**

1. Fungsi `_use_database` dalam kode tersebut bertujuan untuk mengganti basis data yang sedang digunakan (`database_used`) sesuai dengan permintaan pengguna. Pertama, kode melakukan parsing terhadap string permintaan (request) untuk mendapatkan nama basis data yang diminta (`database_name`). Selanjutnya, kode memeriksa apakah basis data dengan nama yang diminta tersebut ada. Jika tidak, respons akan mengindikasikan kesalahan bahwa basis data tidak ada. Jika basis data tersebut ada, langkah berikutnya adalah memeriksa izin akses pengguna terhadap basis data tersebut. Ini dilakukan dengan menggunakan command-line tool 'grep' untuk mencocokkan tuple (`database_name`, `user_client`) dalam file permissions_path. Jika tidak ada kecocokan, respons akan mengindikasikan kesalahan bahwa pengguna tidak memiliki akses untuk menggunakan basis data tersebut. Setelah itu, basis data yang digunakan (`database_used`) diubah sesuai dengan basis data yang diminta, dan respons "USE OK" dikirim. Fungsi ini juga mencatat operasi pengguna dalam log pesan sebelum mengembalikan nilai keluaran. Keseluruhan fungsi ini dirancang untuk memastikan bahwa pengguna hanya dapat menggunakan basis data yang memiliki izin akses.

```c
int _use_database(char* request, char* response) {
    // parsing request
    char database_name[BUFFER_SIZE];

    // ...

    // check database
    char* database_path = format_string("%s/%s", databases_path, database_name);

    if (access(database_path, F_OK) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Database doesn't exist"
        );
        return EXIT_FAILURE;
    }
    free(database_path);

    // check user permission
    char* check_user_permission = format_string(
        "grep -wq '%s,%s' '%s'",
        database_name, user_client, permissions_path
    );

    if (system(check_user_permission) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User has no access to use this database"
        );
        return EXIT_FAILURE;
    }
    free(check_user_permission);

    // change database_used
    if (database_used != NULL) free(database_used);
    database_used = format_string("%s", database_name);

    sprintf(response, "USE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

2. Fungsi `_grant_permission` bertujuan untuk memberikan izin akses pengguna tertentu ke suatu basis data. Pertama, kode melakukan parsing terhadap string permintaan (request) untuk mendapatkan nama basis data (`database_name`) dan nama pengguna (`user_name`). Setelah parsing, langkah selanjutnya adalah memeriksa apakah pengguna tersebut sudah memiliki izin akses ke basis data yang diminta. Ini dilakukan dengan menggunakan command-line tool 'grep' untuk mencocokkan tuple (`database_name`, `user_name`) dalam file `permissions_path`. Jika sudah ada kecocokan, respons akan mengindikasikan kesalahan bahwa pengguna sudah memiliki akses ke basis data tersebut.

```c
int _grant_permission(char* request, char* response) {
    // parsing request
    char database_name[BUFFER_SIZE];
    char user_name[BUFFER_SIZE];

    // ...

    // check user permission
    char* check_user_permission = format_string(
        "grep -wq '%s,%s' '%s'",
        database_name, user_name, permissions_path
    );

    if (system(check_user_permission) == 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User already has access to database"
        );
        return EXIT_FAILURE;
    }
    free(check_user_permission);

    // add user permission
    char* add_user_permission = format_string(
        "echo '%s,%s' >> '%s'",
        database_name, user_name, permissions_path
    );
    system(add_user_permission);
    free(add_user_permission);

    sprintf(response, "GRANT PERMISSION OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Jika belum ada izin akses, langkah selanjutnya adalah menambahkan izin akses untuk pengguna tersebut. Kode menggunakan command-line tool 'echo' untuk menambahkan baris baru yang berisi tuple (`database_name`, `user_name`) ke dalam file permissions_path. Setelah itu, respons "GRANT PERMISSION OK" dikirim, dan operasi ini dicatat dalam log pesan sebelum mengembalikan nilai keluaran. Fungsi ini dirancang untuk memastikan bahwa pengguna hanya mendapatkan izin akses ke suatu basis data jika belum memiliki izin sebelumnya.

**- OUTPUT** 

![autori](/uploads/0a0f6f8da502ca88ee96382778924ffb/autori.png)

### **C. SOAL C<a name="soalc"></a>**

**- DATA DEFINITION LANGUAGE**

1. Fungsi `_create_database` bertujuan untuk membuat basis data baru. Pertama, kode ini melakukan parsing terhadap string permintaan (request) untuk mendapatkan nama basis data yang diinginkan (database_name). Setelah itu, langkah pertama adalah memeriksa apakah basis data dengan nama tersebut sudah ada atau belum. Jika sudah ada, maka fungsi akan mengembalikan pesan kesalahan yang menunjukkan bahwa basis data sudah ada.

```c
int _create_database(char* request, char* response) {
    // parsing request
    char database_name[BUFFER_SIZE];

    char* parse_str = "CREATE DATABASE %[^ ;];";
    int parsed_items = sscanf(request, parse_str, database_name);

    // ...

    // create database
    mkdir(database_path, 0777);
    free(database_path);

    // add user database permission
    char* add_permission;
    if (strcmp(user_client, "root") == 0) add_permission = format_string(
        "echo '%s,%s' >> '%s'",
        database_name, user_client, permissions_path
    );
    else add_permission = format_string(
        "echo '%s,%s\n%s,root' >> '%s'",
        database_name, user_client, database_name, permissions_path
    );
    system(add_permission);
    free(add_permission);

    sprintf(response, "CREATE DATABASE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Jika basis data belum ada, langkah berikutnya adalah membuat direktori untuk basis data baru tersebut dengan menggunakan fungsi mkdir. Dalam proses ini, kode membentuk path lengkap ke direktori basis data dengan menggabungkan databases_path dan database_name.

Setelah membuat direktori basis data, langkah selanjutnya adalah memberikan izin akses ke basis data tersebut. Ini dilakukan dengan menambahkan baris baru ke dalam file yang menyimpan izin akses (permissions_path). Jika pengguna yang membuat basis data adalah root, maka baris yang ditambahkan hanya berisi nama basis data dan nama pengguna root. Jika pengguna bukan root, maka selain menambahkan baris tersebut, juga ditambahkan baris tambahan dengan hak akses root ke basis data tersebut.

2. Fungsi `_create_table` bertujuan untuk membuat tabel baru dalam suatu basis data. Pertama, fungsi ini memeriksa apakah ada basis data yang sedang digunakan dengan menggunakan variabel database_used. Jika tidak ada basis data yang digunakan, fungsi akan mengembalikan pesan kesalahan yang menunjukkan bahwa tidak ada basis data yang sedang digunakan.

```c
int _create_table(char* request, char* response) {
    // ...

    // create table
    FILE* table = fopen(table_path, "w");

    char* column_pair = strtok(constraints, ",");
    while (column_pair != NULL) {
        // ...

        fprintf(table, "%s %s", column_name, column_type);
        if (column_pair != NULL) fprintf(table, ",");
    }
    fprintf(table, "\n");

    fclose(table);
    free(table_path);
    sprintf(response, "CREATE TABLE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Jika basis data belum ada, langkah berikutnya adalah membuat direktori untuk basis data baru tersebut dengan menggunakan fungsi `mkdir`. Dalam proses ini, kode membentuk path lengkap ke direktori basis data dengan menggabungkan `databases_path` dan `database_name`.

Setelah membuat direktori basis data, langkah selanjutnya adalah memberikan izin akses ke basis data tersebut. Ini dilakukan dengan menambahkan baris baru ke dalam file yang menyimpan izin akses (`permissions_path`). Jika pengguna yang membuat basis data adalah root, maka baris yang ditambahkan hanya berisi nama basis data dan nama pengguna root. Jika pengguna bukan root, maka selain menambahkan baris tersebut, juga ditambahkan baris tambahan dengan hak akses root ke basis data tersebut.

Terakhir, fungsi ini mengembalikan pesan sukses "CREATE DATABASE OK" setelah berhasil membuat basis data dan memberikan izin akses. Selain itu, operasi ini dicatat dalam log pesan sebelum fungsi mengembalikan nilai keluaran.

3. Fungsi `_create_table` bertujuan untuk membuat tabel baru dalam suatu basis data. Pertama, fungsi ini memeriksa apakah ada basis data yang sedang digunakan dengan menggunakan variabel `database_used`. Jika tidak ada basis data yang digunakan, fungsi akan mengembalikan pesan kesalahan yang menunjukkan bahwa tidak ada basis data yang sedang digunakan.

Setelah itu, fungsi melakukan parsing terhadap string permintaan (request) untuk mendapatkan nama tabel (`table_name`) dan konstrain (`constraints`) untuk kolom-kolom dalam tabel tersebut. Selanjutnya, fungsi membuat path lengkap ke tabel yang akan dibuat dengan menggabungkan `databases_path`, `database_used`, dan `table_name`.

```c
int _create_table(char* request, char* response) {
    // ...

    // create table
    FILE* table = fopen(table_path, "w");

    char* column_pair = strtok(constraints, ",");
    while (column_pair != NULL) {
        // ...

        fprintf(table, "%s %s", column_name, column_type);
        if (column_pair != NULL) fprintf(table, ",");
    }
    fprintf(table, "\n");

    fclose(table);
    free(table_path);
    sprintf(response, "CREATE TABLE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Selanjutnya, fungsi membuka file tabel tersebut dalam mode penulisan ("w"). Dalam proses ini, fungsi memisahkan konstrain menjadi pasangan nama kolom dan tipe data menggunakan fungsi strtok. Setiap pasangan ini kemudian dituliskan ke file tabel dengan menggunakan fungsi fprintf. Jika masih ada pasangan nama kolom dan tipe data selanjutnya, dipisahkan dengan koma. Setelah semua kolom dituliskan, ditambahkan karakter baris baru ("\n").

Terakhir, file tabel ditutup, dan path tabel yang sudah tidak diperlukan lagi dihapus dari memori dengan menggunakan fungsi free. Fungsi ini mengembalikan pesan sukses "CREATE TABLE OK" setelah berhasil membuat tabel. Selain itu, operasi ini dicatat dalam log pesan sebelum fungsi mengembalikan nilai keluaran.

4. Fungsi `_drop_database` bertujuan untuk menghapus suatu basis data beserta semua tabel yang ada di dalamnya. Pertama, fungsi melakukan parsing terhadap string permintaan (request) untuk mendapatkan nama basis data yang akan dihapus (`database_name`).

Setelah itu, fungsi membuat path lengkap ke basis data yang akan dihapus dengan menggabungkan `databases_path` dan `database_name`. Selanjutnya, fungsi memeriksa izin pengguna terkait basis data tersebut. Jika pengguna tidak memiliki izin untuk menghapus basis data tersebut, fungsi mengembalikan pesan kesalahan yang menunjukkan bahwa pengguna tidak memiliki akses untuk menghapus basis data tersebut.

```c
int _drop_database(char* request, char* response) {
    // ...

    // remove database
    char* remove_database = format_string("rm -rf '%s'", database_path);
    system(remove_database);
    free(remove_database);

    // revoke permission
    char* temp_permission_path = format_string("%s.tmp", permissions_path);

    char* revoke_permission = format_string(
        "grep -v '%s,' '%s' > '%s'",
        database_name, permissions_path, temp_permission_path
    );
    system(revoke_permission);
    free(revoke_permission);

    remove(permissions_path);
    rename(temp_permission_path, permissions_path);
    free(temp_permission_path);

    // change database used
    if (database_used != NULL) free(database_used);
    database_used = NULL;

    sprintf(response, "DROP DATABASE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Jika izin ditemukan, fungsi menghapus seluruh direktori basis data (termasuk semua tabel di dalamnya) dengan menggunakan perintah sistem `rm -rf`. Setelah itu, fungsi melakukan operasi pemrosesan file untuk mencabut izin pengguna pada basis data yang dihapus. Hal ini dilakukan dengan membuat salinan file izin sementara (`temp_permission_path`) yang tidak memasukkan izin yang sesuai dengan basis data yang dihapus. Kemudian, file izin utama dihapus dan salinan sementara tersebut diganti namanya menjadi file izin utama.

Selanjutnya, variabel database_used diatur ke NULL karena basis data yang sedang digunakan telah dihapus. Terakhir, fungsi mengembalikan pesan sukses "DROP DATABASE OK" setelah berhasil menghapus basis data dan tabel-tabel di dalamnya. Sebelum fungsi mengembalikan nilai keluaran, operasi ini dicatat dalam log pesan.

5. Fungsi `_drop_table` bertujuan untuk menghapus suatu tabel dari basis data yang sedang digunakan. Fungsi ini pertama kali memeriksa apakah pengguna sedang menggunakan basis data tertentu (`database_used`). Jika tidak, fungsi mengembalikan pesan kesalahan yang menunjukkan bahwa pengguna tidak sedang menggunakan basis data.

Setelah itu, fungsi melakukan parsing terhadap string permintaan (request) untuk mendapatkan nama tabel yang akan dihapus (table_name). Kemudian, fungsi membuat path lengkap ke tabel yang akan dihapus dengan menggabungkan `databases_path`, `database_used`, dan `table_name`.

```c
int _drop_table(char* request, char* response) {
    // ...

    // remove table
    char* remove_table = format_string("rm -rf '%s'", table_path);
    system(remove_table);
    free(remove_table);
    free(table_path);

    sprintf(response, "DROP TABLE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Selanjutnya, fungsi memeriksa apakah tabel tersebut ada. Jika tabel tidak ditemukan, fungsi mengembalikan pesan kesalahan yang menunjukkan bahwa tabel tidak ada. Jika tabel ditemukan, fungsi menggunakan perintah sistem rm -rf untuk menghapus seluruh direktori tabel beserta isinya.

Setelah itu, variabel table_path yang digunakan untuk menyimpan path lengkap ke tabel di dealokasi. Terakhir, fungsi mengembalikan pesan sukses "DROP TABLE OK" setelah berhasil menghapus tabel. Sebelum fungsi mengembalikan nilai keluaran, operasi ini dicatat dalam log pesan.

6. 
Fungsi `_drop_column` bertujuan untuk menghapus suatu kolom dari suatu tabel di dalam basis data yang sedang digunakan. Pertama-tama, fungsi ini memeriksa apakah pengguna sedang menggunakan suatu basis data (`database_used`). Jika tidak, fungsi mengembalikan pesan kesalahan yang menunjukkan bahwa pengguna tidak sedang menggunakan basis data.

Selanjutnya, fungsi melakukan parsing terhadap string permintaan (request) untuk mendapatkan nama kolom yang akan dihapus (`column_name`) dan nama tabel tempat kolom tersebut berada (`table_name`). Setelah itu, fungsi membuat path lengkap ke tabel yang akan diubah dengan menggabungkan `databases_path`, `database_used`, dan `table_name`.

```c
int _drop_column(char* request, char* response) {
    // ...

    // remove column
    while (fscanf(table, " %[^\n]", row) != EOF) {
        // ...

        fprintf(temp_table, "\n");
    }

    fclose(table);
    fclose(temp_table);

    // rename temp_table;
    remove(table_path);
    rename(temp_table_path, table_path);
    free(temp_table_path);
    free(table_path);

    sprintf(response, "DROP COLUMN OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Fungsi kemudian membuka file tabel (dengan mode "r" untuk membaca) dan membuat file sementara (temp_table) yang akan digunakan untuk menyimpan hasil modifikasi tabel tanpa kolom yang dihapus. Selama membaca baris-baris dari tabel, fungsi memproses setiap baris untuk menghilangkan data yang terkait dengan kolom yang akan dihapus.

Setelah selesai memproses tabel, kedua file ditutup. File tabel asli dihapus menggunakan perintah `remove`, dan file sementara yang telah dimodifikasi diubah namanya menjadi nama file asli menggunakan perintah rename.

Selanjutnya, variabel `temp_table_path` dan `table_path` yang digunakan untuk menyimpan path lengkap ke file sementara dan tabel di dealokasi. Terakhir, fungsi mengembalikan pesan sukses "DROP COLUMN OK" setelah berhasil menghapus kolom. Sebelum fungsi mengembalikan nilai keluaran, operasi ini dicatat dalam log pesan.

**- OUTPUT** 
![ddl](/uploads/b2223d35a4fdca7c1ee825d6ff5bfb50/ddl.png)

### **D. SOAL D (Data Manipulation Language)<a name="soald"></a>**

## D1. Insert

1. Pengecekan Database yang Digunakan

```c
if (database_used == NULL) {
    // Pesan kesalahan jika tidak ada database yang digunakan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: No database used"
    );
    return EXIT_FAILURE;
}
```

Program memeriksa apakah ada database yang sedang digunakan. Jika tidak ada, maka akan menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

2. Penguraian Perintah (Request)

```c
char table_name[BUFFER_SIZE];
char row[BUFFER_SIZE];

char* parse_str = "INSERT INTO %s (%[^)]);";
int parsed_items = sscanf(request, parse_str, table_name, row);
```

Menggunakan fungsi sscanf untuk mengurai perintah request yang diberikan. Perintah ini diharapkan dalam format "INSERT INTO table_name (value_column_1, value_column_2, ...);". Jika penguraian tidak berhasil (jumlah item yang diurai bukan 2), program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

3. Pengecekan Ketersediaan Tabel

```c
char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

if (access(table_path, F_OK) != 0) {
    // Pesan kesalahan jika tabel tidak ditemukan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Table doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Membuat path ke tabel dan memeriksa apakah tabel tersebut ada. Jika tidak ada, program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

4. Penguraian Nilai Baris (Row)

```c
int total_column = 0;
char new_row[BUFFER_SIZE][BUFFER_SIZE];
char* value = strtok(row, ",");
while (value != NULL) {
    // Menghapus spasi ekstra di awal dan akhir nilai
    remove_leading_spaces(value);
    remove_trailing_spaces(value);

    // Menyalin nilai ke dalam array new_row
    strcpy(new_row[total_column++], value);
    value = strtok(NULL, ",");
}
```

Mengurai nilai-nilai kolom dari baris yang akan dimasukkan ke dalam tabel.

5. Pengecekan Tipe Data Kolom

```c
FILE* table = fopen(table_path, "a+");
// ...

char* column_pair = strtok(columns, ",");
while (column_pair != NULL) {
    // ...
    // Pengecekan tipe data kolom
    // ...
}
```

Membuka tabel dan membaca tipe data kolom dari baris pertama tabel. Memeriksa apakah nilai yang akan dimasukkan sesuai dengan tipe data kolom yang diharapkan. Jika tidak sesuai, menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

6. Penambahan Baris ke dalam Tabel

```c
// Menambahkan baris ke dalam tabel
for (int i = 0; i < total_column; i++) {
    if (i != 0) fprintf(table, ",");
    fprintf(table, "%s", new_row[i]);
}
fprintf(table, "\n");

fclose(table);
free(table_path);
```

Membuka tabel untuk ditambahkan (mode "a+"). Menambahkan nilai-nilai baris baru ke dalam tabel. Menutup file tabel dan membebaskan memori yang digunakan untuk path tabel.

7. Pesan Balasan dan Logging

```c
sprintf(response, "INSERT OK\n");
message_log(request);
return EXIT_SUCCESS;
```

Menghasilkan pesan balasan yang menyatakan bahwa operasi "INSERT" berhasil. Merekam permintaan (request) ke dalam log.

Berikut ini adalah hasil dari perintah INSERT:

![INSERT](/uploads/562b498847a3e41f3fbbe4a7d745ec72/INSERT.PNG)


## D2. Update

1. Pengecekan Database yang Digunakan

```c
if (database_used == NULL) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: No database used"
    );
    return EXIT_FAILURE;
}
```

Program memeriksa apakah ada database yang sedang digunakan. Jika tidak ada, maka akan menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

2. Penguraian Perintah (Request)

```c
char table_name[BUFFER_SIZE];
char column_name[BUFFER_SIZE];
char value[BUFFER_SIZE];
char where_column[BUFFER_SIZE];
char where_value[BUFFER_SIZE];
bool has_where = false;

char* parse_str = "UPDATE %s SET %[^=]= %[^;W]WHERE %[^=]= %[^;];";
int parsed_items = sscanf(
    request, parse_str,
    table_name, column_name, value, where_column, where_value
);
```

Menggunakan fungsi sscanf untuk mengurai perintah request yang diberikan. Perintah ini diharapkan dalam format "UPDATE table_name SET column_name=value [WHERE where_column=where_value];". Jika penguraian tidak berhasil (jumlah item yang diurai bukan 3 atau 5), program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

3. Pengecekan Ketersediaan Tabel

```c
char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

if (access(table_path, F_OK) != 0) {
    // Pesan kesalahan jika tabel tidak ditemukan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Table doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Membuat path ke tabel dan memeriksa apakah tabel tersebut ada. Jika tidak ada, program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

4. Pembukaan File Tabel

```c
char* temp_table_path = format_string("%s.tmp", table_path);

FILE* table = fopen(table_path, "r");
FILE* temp_table = fopen(temp_table_path, "w");
```

Membuka file tabel untuk dibaca (table) dan file temporary untuk ditulis (temp_table).

5. Pengecekan dan Pemrosesan Kolom

```c
// ...
while (column_pair != NULL) {
    // ...
    // Pengecekan tipe data kolom
    // ...
}
```

Pengecekan tipe data kolom yang mirip dengan pada fungsi _insert. Memproses kolom dan menuliskan ke file temporary, serta melakukan validasi tipe data.

6. Pengecekan Keberadaan Kolom yang Diperbarui dan Kolom Kondisi (WHERE)

```c
if (column_index == -1) {
    // Pesan kesalahan jika kolom tidak ditemukan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Column doesn't exist"
    );
    fclose(table);
    fclose(temp_table);
    return EXIT_FAILURE;
}

if (has_where && where_column_index == -1) {
    // Pesan kesalahan jika kolom kondisi tidak ditemukan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Where column doesn't exist"
    );
    fclose(table);
    fclose(temp_table);
    return EXIT_FAILURE;
}
```

Program memastikan bahwa kolom yang akan diperbarui dan kolom kondisi (WHERE) tersedia di dalam tabel. Jika tidak, program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

7. Pemrosesan Baris-Baris Tabel

```c
while (fscanf(table, " %[^\n]", row) != EOF) {
    // Pemrosesan baris-baris tabel untuk melakukan update
    // ...
}
```

Melakukan iterasi terhadap setiap baris di dalam tabel. Memeriksa apakah baris tersebut memenuhi kondisi WHERE (jika ada), dan melakukan perubahan pada nilai kolom yang diinginkan.

8. Pembaruan File Tabel dan Penamaan Ulang

```c
// rename temp_table;
remove(table_path);
rename(temp_table_path, table_path);
free(temp_table_path);
free(table_path);
```

Menyimpan perubahan pada file temporary ke dalam file tabel utama. Menghapus file temporary dan mengganti nama file temporary menjadi nama file tabel utama.

9. Pesan Balasan dan Logging

```c
sprintf(response, "UPDATE OK\n");
message_log(request);
return EXIT_SUCCESS;
```

Menghasilkan pesan balasan yang menyatakan bahwa operasi "UPDATE" berhasil. Merekam permintaan (request) ke dalam log.

Berikut ini adalah hasil dari perintah UPDATE:

![UPDATE](/uploads/532b590fdbd59c9937defee6edcebcd5/UPDATE.PNG)


## D3. Delete

1. Pengecekan Database yang Digunakan

```c
if (database_used == NULL) {
    // Pesan kesalahan jika tidak ada database yang digunakan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: No database used"
    );
    return EXIT_FAILURE;
}
```
Program memeriksa apakah ada database yang sedang digunakan. Jika tidak ada, maka akan menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

2. Penguraian Perintah (Request)

```c
char table_name[BUFFER_SIZE];
char where_column[BUFFER_SIZE];
char where_value[BUFFER_SIZE];
bool has_where = false;

char* parse_str = "DELETE FROM %[^;W]WHERE %[^=]= %[^;];";
int parsed_items = sscanf(
    request, parse_str,
    table_name, where_column, where_value
);
```

Menggunakan fungsi sscanf untuk mengurai perintah request yang diberikan. Perintah ini diharapkan dalam format "DELETE FROM table_name [WHERE column_name=value];". Jika penguraian tidak berhasil (jumlah item yang diurai bukan 1 atau 3), program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

3. Pengecekan Ketersediaan Tabel

```c
char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

if (access(table_path, F_OK) != 0) {
    // Pesan kesalahan jika tabel tidak ditemukan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Table doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Membuat path ke tabel dan memeriksa apakah tabel tersebut ada. Jika tidak ada, program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

4. Pembukaan File Tabel

```c
char* temp_table_path = format_string("%s.tmp", table_path);

FILE* table = fopen(table_path, "r");
FILE* temp_table = fopen(temp_table_path, "w");
```

Membuka file tabel untuk dibaca (table) dan file temporary untuk ditulis (temp_table).

5. Pengecekan dan Pemrosesan Kolom

```c
// ...
while (column_pair != NULL) {
    // ...
    // Pengecekan tipe data kolom
    // ...
}
```

Pengecekan tipe data kolom yang mirip dengan pada fungsi _insert. Menyalin kolom ke file temporary dan melakukan validasi tipe data.

6. Pengecekan Keberadaan Kolom Kondisi (WHERE)

```c
if (has_where && where_column_index == -1) {
    // Pesan kesalahan jika kolom kondisi tidak ditemukan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Where column doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Program memastikan bahwa kolom kondisi (WHERE) tersedia di dalam tabel. Jika tidak, program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

7. Pemrosesan Baris-Baris Tabel dan Penghapusan Baris

```c
while (fscanf(table, " %[^\n]", row) != EOF) {
    // Pemrosesan baris-baris tabel untuk melakukan delete
    // ...
}

Melakukan iterasi terhadap setiap baris di dalam tabel. Memeriksa apakah baris tersebut memenuhi kondisi WHERE (jika ada), dan menghapus baris tersebut jika ditemukan.

8. Pembaruan File Tabel dan Penamaan Ulang

```c
// rename temp_table;
remove(table_path);
rename(temp_table_path, table_path);
free(temp_table_path);
free(table_path);
```
Menyimpan perubahan pada file temporary ke dalam file tabel utama. Menghapus file temporary dan mengganti nama file temporary menjadi nama file tabel utama.

9. Pesan Balasan dan Logging

```c
sprintf(response, "DELETE OK\n");
message_log(request);
return EXIT_SUCCESS;
```

Menghasilkan pesan balasan yang menyatakan bahwa operasi "DELETE" berhasil. Merekam permintaan (request) ke dalam log.

Berikut ini adalah hasil dari perintah DELETE:

![DELETE](/uploads/5c4f332f1383c8f90ae90dcd0b7e3e7f/DELETE.PNG)


## D4. Select

1. Pengecekan Database yang Digunakan

```c
if (database_used == NULL) {
    // Pesan kesalahan jika tidak ada database yang digunakan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: No database used"
    );
    return EXIT_FAILURE;
}
```

Program memeriksa apakah ada database yang sedang digunakan. Jika tidak ada, maka akan menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

2. Penguraian Perintah (Request)

```c
char select_columns[BUFFER_SIZE];
char table_name[BUFFER_SIZE];
char where_column[BUFFER_SIZE];
char where_value[BUFFER_SIZE];
bool select_all = false;
bool has_where = false;

char* parse_str = "SELECT %[^F]FROM %[^ ;]%*[; ] WHERE %[^=]= %[^;];";
int parsed_items = sscanf(
    request, parse_str,
    select_columns, table_name, where_column, where_value
);
```

Menggunakan fungsi sscanf untuk mengurai perintah request yang diberikan. Perintah ini diharapkan dalam format "SELECT column_name FROM table_name [WHERE column_name=value];". Jika penguraian tidak berhasil (jumlah item yang diurai bukan 2 atau 4), program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

3. Pengecekan Kolom yang Dipilih dan WHERE

```c
if (has_where) remove_trailing_spaces(where_value);
remove_trailing_spaces(select_columns);
```

Menghilangkan spasi di belakang nilai pada klausa WHERE dan kolom yang dipilih.

4. Penyusunan Path Tabel dan Pengecekan Ketersediaan Tabel

```c
char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

if (access(table_path, F_OK) != 0) {
    // Pesan kesalahan jika tabel tidak ditemukan
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Table doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Membuat path ke tabel dan memeriksa apakah tabel tersebut ada. Jika tidak ada, program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

5. Pembukaan File Tabel

```c
FILE* table = fopen(table_path, "r");
```

Membuka file tabel untuk dibaca (table).

6. Pengecekan dan Pemrosesan Kolom

```c
int arg_column = 0;
int total_column = 0;
int column_id[BUFFER_SIZE];
char columns[BUFFER_SIZE][BUFFER_SIZE];
char row[BUFFER_SIZE];
fscanf(table, " %[^\n]", row);

strcpy(response, "");
if (select_all) {
    strcat(response, row);
}
else {
    char* column_name = strtok(select_columns, ",");
    while (column_name != NULL) {
        remove_leading_spaces(column_name);
        remove_trailing_spaces(column_name);

        strcpy(columns[arg_column++], column_name);
        column_name = strtok(NULL, ",");
    }
    for (int i = 0; i < arg_column; i++) {
        int index = 0;
        char row_copy[BUFFER_SIZE];
        strcpy(row_copy, row);

        char* column_pair = strtok(row_copy, ",");
        while (column_pair != NULL) {
            char column[BUFFER_SIZE];
            sscanf(column_pair, "%s", column);
            if (strcmp(column, columns[i]) == 0) {
                column_id[total_column++] = index;
                if (i != 0) strcat(response, ",");
                strcat(response, column_pair);
                break;
            }
            column_pair = strtok(NULL, ",");
            index++;
        }
    }
}
strcat(response, "\n");
```

Memproses kolom yang dipilih. Jika kolom yang dipilih adalah '*', maka seluruh kolom akan dipilih. Jika tidak, program akan memproses kolom yang diinginkan. Menyusun baris respons dengan kolom yang dipilih.

7. Pengecekan Kolom Kondisi (WHERE)

```c
int column_index = 0;
int where_column_index = -1;
char* column_pair = strtok(row, ",");
if (has_where) {
    while (column_pair != NULL) {
        char column_name[BUFFER_SIZE];
        char column_type[BUFFER_SIZE];
        sscanf(column_pair, "%s %s", column_name, column_type);

        if (strcmp(column_name, where_column) == 0) {
            where_column_index = column_index;
            if (
                strcmp(column_type, "int") == 0 &&
                !is_int(where_value)
            ) {
                sprintf(
                    response, "%s %s is not a valid int\n",
                    "\e[91mError\e[39m:", where_value
                );
                return EXIT_FAILURE;
            }
            else if (
                strcmp(column_type, "string") == 0 &&
                !is_string(where_value)
            ) {
                sprintf(
                    response, "%s %s is not a valid string\n%s\n",
                    "\e[91mError\e[39m:", where_value,
                    "'\e[3mexample string\e[0m' is a valid string"
                );
                return EXIT_FAILURE;
            }
        }

        column_pair = strtok(NULL, ",");
        column_index++;
    }
}
```

Memeriksa keberadaan dan tipe data kolom kondisi (WHERE). Jika tidak valid, program menghasilkan pesan kesalahan dan keluar dengan status EXIT_FAILURE.

8. Pengecekan Validitas Seleksi Kolom

```c
if (!select_all && total_column != arg_column) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Select has invalid column/s"
    );
    return EXIT_FAILURE;
}
```

Memastikan bahwa semua kolom yang dipilih adalah valid.

9. Seleksi dan Pemrosesan Baris Tabel

```c
while (fscanf(table, " %[^\n]", row) != EOF) {
    bool is_valid_row = !has_where;
    if (has_where) {
        int index = 0;
        char row_copy[BUFFER_SIZE];
        strcpy(row_copy, row);
        char* cell = strtok(row_copy, ",");
        while (cell != NULL) {
            if (
                index == where_column_index &&
                strcmp(cell, where_value) == 0
            ) {
                is_valid_row = true;
                break;
            }
            cell = strtok(NULL, ",");
            index++;
        }
    }
    if (is_valid_row) {
        if (select_all) strcat(response, row);
        else {
            for (int i = 0; i < total_column; i++) {
                int index = 0;
                char row_copy[BUFFER_SIZE];
                strcpy(row_copy, row);
                char* cell = strtok(row_copy, ",");
                while (cell != NULL) {
                    if (index == column_id[i]) {
                        if (i != 0) strcat(response, ",");
                        strcat(response, cell);
                        break;
                    }
                    cell = strtok(NULL, ",");
                    index++;
                }
            }
        }
        strcat(response, "\n");
    }
}
```

Melakukan iterasi terhadap setiap baris di dalam tabel. Memeriksa apakah baris tersebut memenuhi kondisi WHERE (jika ada), dan memproses baris tersebut sesuai dengan kolom yang dipilih.

10. Penutup dan Logging

```c
fclose(table);
free(table_path);

message_log(request);
return EXIT_SUCCESS;
```

Menutup file tabel dan melepaskan memori dari path tabel. Merekam permintaan (request) ke dalam log. Mengembalikan status EXIT_SUCCESS sebagai tanda operasi "SELECT" berhasil.

Berikut ini adalah hasil dari perintah SELECT:

![SELECT](/uploads/ac4e573a28a78e7d8ee5f3a6a61cffe1/SELECT.PNG)


Berikut ini adalah hasil dari perintah WHERE:

![WHERE](/uploads/4a432b8df3e26923b92e795e4e13c3e5/WHERE.PNG)


### **E. SOAL E (LOGGING)<a name="soale"></a>**

1. Pembentukan Path File Log

```c
char log_file_path[BUFFER_SIZE];
sprintf(log_file_path, "%s/%s.log", log_path, database_used);
```

Membentuk path lengkap ke file log dengan menggunakan sprintf. Path ini terdiri dari direktori log (log_path), nama database yang sedang digunakan (database_used), dan ekstensi file log (".log").

2. Pengalokasian Memori untuk Pesan Log

```c
char *log_message = malloc(5000 * sizeof(char));

Mengalokasikan memori untuk menyimpan pesan log. Ukuran alokasi memori diatur sebesar 5000 karakter.

3. Pengambilan Waktu Sekarang

```c
time_t now = time(NULL);
struct tm *t = localtime(&now);

int year = t->tm_year;
int month = t->tm_mon;
int day = t->tm_mday;
int hour = t->tm_hour;
int minute = t->tm_min;
int second = t->tm_sec;
```

Menggunakan fungsi time untuk mendapatkan waktu saat ini. Menggunakan struktur tm untuk mendapatkan informasi tahun, bulan, hari, jam, menit, dan detik.

4. Pembentukan Pesan Log

```c
sprintf(log_message, "echo '%d-%02d-%02d %02d:%02d:%02d:%s:%s' >> '%s'\n", year + 1900, month + 1, day, hour, minute, second, user_client, command, log_file_path);
```

Menggunakan sprintf untuk membentuk pesan log dengan format tertentu. Pesan log terdiri dari informasi waktu (tahun, bulan, hari, jam, menit, detik), nama pengguna klien (user_client), perintah yang dilakukan (command), dan path file log.

5. Eksekusi Pesan Log

```c
system(log_message);
```

Menggunakan fungsi system untuk mengeksekusi pesan log. Pesan log tersebut berisi perintah echo yang menambahkan informasi log ke file log.

6. Pengembalian Nilai dan Penutup

```c
return EXIT_SUCCESS;
```

Mengembalikan status EXIT_SUCCESS sebagai tanda bahwa penulisan log berhasil.

Berikut ini adalah hasil log nya:

![LOG](/uploads/7f0740d67d2b40722071c39fd836a924/LOG.PNG)


### **F. SOAL F (RELIABIILTY)<a name="soalf"></a>**

1. Struktur Permission

```c
struct permission
{
  char name[10000];
  char pass[10000];
};
```

Struktur ini mendefinisikan format data untuk menyimpan nama pengguna (name) dan kata sandi (pass).

2. Fungsi checkPermission

```c
int checkPermission(const char *uname, const char *pass)
{
  FILE *file = fopen("../database/databases/databaseusers/users", "r");

  if (file == NULL)
  {
    perror("Error opening the file");
    return 0;
  }

  char line[200];
  int mark = 0;

  while (fgets(line, sizeof(line), file))
  {
    char temp_uname[100], temp_pass[100];

    if (sscanf(line, "%99[^,], %99[^\n]", temp_uname, temp_pass) == 2)
    {
      if (strcmp(temp_uname, uname) == 0 && strcmp(temp_pass, pass) == 0)
      {
        mark = 1;
        break;
      }
    }
  }

  fclose(file);

  if (mark == 0)
  {
    printf("Not Allowed: No permission\n");
    return 0;
  }
  else
    return 1;
}
```

Fungsi ini digunakan untuk memeriksa izin berdasarkan nama pengguna dan kata sandi. Data izin dibaca dari file ../database/databases/databaseusers/users.

3. Fungsi copyToBackup

```c
void copyToBackup(const char *logFileName, const char *backupFileName)
{
  FILE *logFile = fopen(logFileName, "r");
  FILE *backupFile = fopen(backupFileName, "w");

  if (logFile == NULL || backupFile == NULL)
  {
    perror("Error opening files");
    return;
  }

  char buffer[20000];
  while (fgets(buffer, sizeof(buffer), logFile))
  {
    char temp_buff[20000];
    snprintf(temp_buff, sizeof temp_buff, "%s", buffer);

    // Extracting action part after the last colon ':'
    char *lastColon = strrchr(temp_buff, ':');
    if (lastColon != NULL)
    {
      // Writing only the action part to the backup file
      fputs(lastColon + 1, backupFile);
    }
  }

  fclose(logFile);
  fclose(backupFile);
}
```

Fungsi ini mengcopy isi file log (logFileName) ke file backup (backupFileName), tetapi hanya bagian aksi (action) setelah titik dua terakhir.

4. Fungsi main

```c
int main(int argc, char *argv[])
{
  if (argc < 6)
  {
    fprintf(stderr, "Usage: %s <arg1> <arg2> <arg3> <arg4> <arg5>\n", argv[0]);
    return 1;
  }
```

Program memeriksa apakah jumlah argumen yang diberikan sesuai atau tidak. Jika kurang dari 6, program menampilkan pesan usage dan keluar.

5. Pemeriksaan Izin

```c
int allowed = 0;

if (geteuid() == 0)
  allowed = 1;
else
  allowed = checkPermission(argv[2], argv[4]);

if (allowed == 0)
  return 0;
```

Jika program dijalankan sebagai root (geteuid() == 0), maka diberikan izin (allowed = 1). Jika tidak, maka fungsi checkPermission digunakan untuk memeriksa izin berdasarkan argumen ke-2 (nama pengguna) dan argumen ke-4 (kata sandi).

6. Penentuan Lokasi File Log dan Backup

```c
char logFileLoc[10000];
snprintf(logFileLoc, sizeof logFileLoc, "../database/databases/log/%s.log", argv[5]);

char backupFileLoc[10000];
snprintf(backupFileLoc, sizeof backupFileLoc, "%s.backup", argv[5]);
```

Menentukan lokasi file log dan file backup berdasarkan argumen ke-5.

7. Copy Log ke Backup

```c
copyToBackup(logFileLoc, backupFileLoc);
```

Memanggil fungsi copyToBackup untuk menyalin isi file log ke file backup.

8. Proses Input

```c
FILE *file = stdin;

if (file == NULL)
{
  perror("Error opening the input file");
  return 0;
}
```

Membuka file input dari stdin (standar input).

```c
char buff[20000];
int mark = 0;

while (fgets(buff, sizeof(buff), file))
{
  // ...
}
```

Loop untuk membaca perintah dari file input.

```c
fclose(file);
```

Menutup file input.

9. Pengolahan Perintah

```c
char *token;
char temp_buff[20000];
snprintf(temp_buff, sizeof temp_buff, "%s", buff);
token = strtok(temp_buff, ":");

int i = 0;
char cmd[100][10000];

while (token != NULL && i < 100)
{
  strcpy(cmd[i], token);
  i++;
  token = strtok(NULL, ":");
}
```

Memisahkan perintah menjadi token-token dan menyimpannya dalam array cmd.

10. Pengecekan Perintah USE

```c
if (i >= 5)
{
  char *b = strstr(cmd[4], "USE");
  if (b != NULL)
  {
    // ...
  }
}
```

Jika jumlah token lebih dari atau sama dengan 5, program mengecek apakah perintah mengandung kata kunci "USE".

```c
char *tokens;
char temp_cmd[20000];
strcpy(temp_cmd, cmd[4]);
tokens = strtok(temp_cmd, "; ");

int j = 0;
char use_cmd[100][10000];

while (tokens != NULL && j < 100)
{
  strcpy(use_cmd[j], tokens);
  j++;
  tokens = strtok(NULL, "; ");
}

if (j >= 2 && strcmp(use_cmd[1], argv[5]) == 0)
  mark = 1;
else
  mark = 0;
```

Jika perintah mengandung "USE", program memisahkan token dan menyimpannya dalam array use_cmd. Jika jumlah token cukup dan database yang di-"USE" sesuai dengan argumen ke-5, variabel mark diubah menjadi 1.

11. Output Hasil

```c
if (mark == 1)
  printf("%s", buff);
```

Jika variabel mark bernilai 1, program mencetak perintah ke layar.

Berikut ini adalah isi dari backup nya:

![RELIABILITY_DUMP](/uploads/f03460d34bce040b71e3fcb14b9133c0/RELIABILITY_DUMP.PNG)


### **G. SOAL G (TAMBAHAN)<a name="soalg"></a>**

Command dapat dimasukkan lewat file dengan redirection di program client.

Berikut ini adalah screenshotnya:

![G_TAMBAHAN](/uploads/97a691b97f249680af144bae4d5c9987/G_TAMBAHAN.PNG)


### **H. SOAL H (ERROR HANDLING)<a name="soalh"></a>**

Berikut ini adalah beberapa error handlingnya:

![ERROR_HANDLING](/uploads/9ee2f6c8086a85d45abbfcdb8c7726d1/ERROR_HANDLING.PNG)


### **I. Penjelasan Program Cron<a name="cronsh"></a>**

`cron.sh` pada program inni dirancang untuk melakukan pencadangan (backup) terhadap database-database yang terdapat dalam direktori /home/adfi/FP/database/databases. Setiap iterasi melibatkan sebuah database, dan skrip mengidentifikasi nama database tersebut. Skrip kemudian membangun perintah untuk menjalankan eksekutable database yang disimpan di lokasi /home/adfi/FP/database/databases dengan argumen "BACKUP" dan nama database sebagai parameter. Hasil dari eksekusi perintah backup ini disimpan dalam variabel backup_result. 

```sh
#!/bin/bash

DATABASE_EXECUTABLE="/home/adfi/FP/database/databases"

LOG_PATH="/home/adfi/FP/database/databases/log"

for database in /home/adfi/FP/database/databases/*; do
    if [ -d "$database" ]; then
        db_name=$(basename "$database")

        backup_command="$DATABASE_EXECUTABLE BACKUP $db_name"

        backup_result=$($backup_command)

        if [ "$backup_result" == "EXIT_SUCCESS" ]; then
            log_file="$LOG_PATH/$db_name-$(date +"%Y%m%d%H%M%S").log"

            echo "$backup_result" > "$log_file"

            echo "Backup completed on $(date)" >> "$log_file"

            $DATABASE_EXECUTABLE/client_dump root $DATABASE_EXECUTABLE log/$db_name.log database$/$db_name

            echo "Dump data saved to .backup file on $(date)" >> "$log_file"
        else
            echo "Backup failed for database $db_name. Error: $backup_result"
        fi
    fi
done
```

Jika hasilnya adalah "EXIT_SUCCESS," skrip membuat file log dengan timestamp yang mencatat berhasilnya pencadangan, dan selanjutnya menjalankan perintah untuk mendump data dari database menggunakan eksekutable client database. Informasi mengenai dump data juga dicatat dalam file log tersebut. Jika pencadangan gagal, skrip mencetak pesan kesalahan ke konsol. Skrip ini membantu otomatisasi proses pencadangan dan logging untuk setiap database yang terdapat dalam direktori yang telah ditentukan.
