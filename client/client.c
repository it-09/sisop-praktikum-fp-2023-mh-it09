#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_SIZE 1024

#define ADDRESS "127.0.0.1"
#define PORT 8080

char* format_string(const char* format, ...) {
    va_list args;
    va_start(args, format);

    size_t length = vsnprintf(NULL, 0, format, args) + 1;
    va_end(args);

    va_start(args, format);
    char* buffer = (char*) malloc(length * sizeof(char));
    vsnprintf(buffer, length, format, args);
    va_end(args);
    
    return buffer;
}

void handle_connection_error(int sock) {
    close(sock);
    perror("Connection error");
    exit(EXIT_FAILURE);
}

void send_data(int sock, const char* data) {
    if (send(sock, data, strlen(data), 0) < 0) {
        handle_connection_error(sock);
    }
}

void receive_data(int sock, char* buffer) {
    if (recv(sock, buffer, BUFFER_SIZE, 0) < 0) {
        handle_connection_error(sock);
    }
}

int main(int argc, char const *argv[]) {
    int sock = 0;
    struct sockaddr_in serv_addr;

    // Membuat socket
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("Socket creation error");
        return EXIT_FAILURE;
    }

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr) <= 0) {
        perror("Invalid address or address not supported");
        close(sock);
        return EXIT_FAILURE;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        handle_connection_error(sock);
    }

    char user_name[BUFFER_SIZE];
    char user_password[BUFFER_SIZE];

    if (geteuid() == 0) {
        strcpy(user_name, "root");
    } else if (argc != 5 || (argc == 5 && strcmp(argv[1], "-u") != 0 && strcmp(argv[3], "-p") != 0)) {
        fprintf(stderr, "\e[91mError\e[39m: Invalid command\nUsage: %s -u <user_name> -p <user_password>\n", argv[0]);
        close(sock);
        return EXIT_FAILURE;
    } else {
        strcpy(user_name, argv[2]);
        strcpy(user_password, argv[4]);
    }

    char* auth_request = format_string("%s;\nAUTH USER %s IDENTIFIED BY %s;", user_name, user_name, user_password);

    send_data(sock, auth_request);

    char response[BUFFER_SIZE];
    receive_data(sock, response);

    if (!strstr(response, "OK")) {
        fprintf(stderr, "%s\n", response);
        free(auth_request);
        close(sock);
        return EXIT_FAILURE;
    }

    while (1) {
        char command[BUFFER_SIZE];

        if (scanf(" %[^\n]", command) == EOF) {
            break;
        }

        char* command_request = format_string("%s;\n%s", user_name, command);

        send_data(sock, command_request);

        receive_data(sock, response);

        printf("%s\n", response);

        free(command_request);
    }

    close(sock);
    free(auth_request);

    return 0;
}

