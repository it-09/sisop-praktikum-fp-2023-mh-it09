#!/bin/bash

DATABASE_EXECUTABLE="/home/adfi/FP/database/databases"

LOG_PATH="/home/adfi/FP/database/databases/log"

for database in /home/adfi/FP/database/databases/*; do
    if [ -d "$database" ]; then
        db_name=$(basename "$database")

        backup_command="$DATABASE_EXECUTABLE BACKUP $db_name"

        backup_result=$($backup_command)

        if [ "$backup_result" == "EXIT_SUCCESS" ]; then
            log_file="$LOG_PATH/$db_name-$(date +"%Y%m%d%H%M%S").log"

            echo "$backup_result" > "$log_file"

            echo "Backup completed on $(date)" >> "$log_file"

            $DATABASE_EXECUTABLE/client_dump root $DATABASE_EXECUTABLE log/$db_name.log database$/$db_name

            echo "Dump data saved to .backup file on $(date)" >> "$log_file"
        else
            echo "Backup failed for database $db_name. Error: $backup_result"
        fi
    fi
done
