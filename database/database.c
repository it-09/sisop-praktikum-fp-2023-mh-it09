#define _GNU_SOURCE
#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>
#include <time.h>

#define PORT 8081
#define BUFFER_SIZE 1024
#define MAXDATALEN 200

char *user_client;
char *database_used;
const char *databases_path = "databases";
const char *databaseusers_path = "databases/databaseusers";
const char *permissions_path = "databases/databaseusers/permissions";
const char *users_path = "databases/databaseusers/users";
const char *log_path = "databases/log";

void server_db() {
    check_and_create_directory(databases_path);
    check_and_create_directory(log_path);
    if (access(databaseusers_path, F_OK) == 0) return;
    create_directory(databaseusers_path);
    create_users_file();
    create_permissions_file();
}

void check_and_create_directory(char* path) {
    if (access(path, F_OK) != 0) {
        mkdir(path, 0777) == 0;
    }
}

void create_directory(char* path) {
    mkdir(path, 0777);
}

void create_users_file() {
    char create_users_file[BUFFER_SIZE];
    sprintf(
        create_users_file, "echo '%s,%s' > %s",
        "user string", "password string", users_path);
    system(create_users_file);
}

void create_permissions_file() {
    char create_permissions_file[BUFFER_SIZE];
    sprintf(
        create_permissions_file, "echo '%s,%s\n%s,%s' > %s",
        "database string", "user string",
        "databaseusers", "root", permissions_path
    );
    system(create_permissions_file);
}


char* format_string(const char* format, ...) {
    va_list args;
    va_start(args, format);

    size_t length = vsnprintf(NULL, 0, format, args) + 1;
    va_end(args);

    va_start(args, format);
    char* buffer = (char*) malloc(length * sizeof(char));
    vsnprintf(buffer, length, format, args);
    va_end(args);
    
    return buffer;
}

void hapus_space(char* str) {
    if (str == NULL) return;
    int i = strlen(str) - 1;
    while (i >= 0 && isspace(str[i])) str[i--] = '\0';
}

void hapus_space_depan(char* str) {
    if (str == NULL) return;
    int len = strlen(str);
    int i = 0, j = 0;
    while (i < len && isspace(str[i])) i++;
    while (i < len) str[j++] = str[i++];
    str[j] = '\0';
}

bool is_int(char* str) {
    int i = 0;
    while (str[i] != '\0') {
        if (!isdigit(str[i++])) return false;
    }
    return true;
}

bool is_string(char* str) {
    if (str == NULL) return false;

    int len = strlen(str);
    if (str[0] != '\'' || str[len-1] != '\'') return false;

    int i = 0;
    while (str[i] != '\0') {
        if (str[i] == '\'' && i != 0 && i != len-1) return false;
        i++;
    }
    return true;
}

int pesan_log(char *command) {
    char log_file_path[BUFFER_SIZE];
    sprintf(log_file_path, "%s/%s.log", log_path, database_used);

    char *log_message = malloc(5000 * sizeof(char));
    time_t now = time(NULL);
    struct tm *t = localtime(&now);

    int year = t->tm_year;
    int month = t->tm_mon;
    int day = t->tm_mday;
    int hour = t->tm_hour;
    int minute = t->tm_min;
    int second = t->tm_sec;

    sprintf(log_message, "echo '%d-%02d-%02d %02d:%02d:%02d:%s:%s' >> '%s'\n", year + 1900, month + 1, day, hour, minute, second, user_client, command, log_file_path);
    system(log_message);

    return EXIT_SUCCESS;
}

int _auth_user(char *request, char *response) {
    char user_name[BUFFER_SIZE];
    char user_password[BUFFER_SIZE];

    char* parse_str = "AUTH USER %s IDENTIFIED BY %[^ ;];";
    int parsed_items = sscanf(request, parse_str, user_name, user_password);

    if (strcmp(user_name, "root") == 0) {
        sprintf(response, "AUTH USER OK\n");
        return EXIT_SUCCESS;
    }

    char* check_user = format_string(
        "grep -wq '%s,%s' '%s'",
        user_name, user_password, users_path
    );

    if (system(check_user) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User is not registered or wrong password"
        );
        return EXIT_FAILURE;
    }
    free(check_user);

    sprintf(response, "AUTH USER OK\n");
    return EXIT_SUCCESS;
}

int _create_user(char* request, char*response) {
    char user_name[BUFFER_SIZE];
    char user_password[BUFFER_SIZE];

    char* parse_str = "CREATE USER %s IDENTIFIED BY %[^ ;];";
    int parsed_items = sscanf(request, parse_str, user_name, user_password);

    if (parsed_items != 2) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "CREATE USER \e[3muser_name\e[0m IDENTIFIED BY \e[3muser_password\e[0m;"
        );
        return EXIT_FAILURE;
    }

    char* check_user = format_string(
        "grep -wq '%s,%s' '%s'",
        user_name, user_password, users_path
    );

    if (system(check_user) == 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User already exists"
        );
        return EXIT_FAILURE;
    }
    free(check_user);

    char* add_user = format_string(
        "echo '%s,%s' >> '%s'",
        user_name, user_password, users_path
    );
    system(add_user);
    free(add_user);

    sprintf(response, "CREATE USER OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _use_database(char* request, char* response) {
    char database_name[BUFFER_SIZE];

    char* parse_str = "USE %[^ ;];";
    int parsed_items = sscanf(request, parse_str, database_name);

    if (parsed_items != 1) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "USE \e[3mdatabase_name\e[0m;"
        );
        return EXIT_FAILURE;
    }

    char* database_path = format_string("%s/%s", databases_path, database_name);

    if (access(database_path, F_OK) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Database doesn't exist"
        );
        return EXIT_FAILURE;
    }
    free(database_path);

    char* check_user_permission = format_string(
        "grep -wq '%s,%s' '%s'",
        database_name, user_client, permissions_path
    );

    if (system(check_user_permission) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User has no access to use this database"
        );
        return EXIT_FAILURE;
    }
    free(check_user_permission);
    
    if (database_used != NULL) free(database_used);
    database_used = format_string("%s", database_name);

    sprintf(response, "USE OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _grant_permission(char* request, char* response) {
    char database_name[BUFFER_SIZE];
    char user_name[BUFFER_SIZE];

    char* parse_str = "GRANT PERMISSION %s INTO %[^ ;];";
    int parsed_items = sscanf(request, parse_str, database_name, user_name);

    if (parsed_items != 2) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "GRANT PERMISSION \e[3mdatabase_name\e[0m INTO \e[3muser_name\e[0m;"
        );
        return EXIT_FAILURE;
    }

    // check user permission
    char* check_user_permission = format_string(
        "grep -wq '%s,%s' '%s'",
        database_name, user_name, permissions_path
    );

    if (system(check_user_permission) == 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User already has access to database"
        );
        return EXIT_FAILURE;
    }
    free(check_user_permission);

    // add user permission
    char* add_user_permission = format_string(
        "echo '%s,%s' >> '%s'",
        database_name, user_name, permissions_path
    );
    system(add_user_permission);
    free(add_user_permission);

    sprintf(response, "GRANT PERMISSION OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _create_database(char* request, char* response) {
    // parsing request
    char database_name[BUFFER_SIZE];

    char* parse_str = "CREATE DATABASE %[^ ;];";
    int parsed_items = sscanf(request, parse_str, database_name);

    if (parsed_items != 1) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "CREATE DATABASE \e[3mdatabase_name\e[0m;"
        );
        return EXIT_FAILURE;
    }

    // check database
    char* database_path = format_string("%s/%s", databases_path, database_name);

    if (access(database_path, F_OK) == 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Database already exists"
        );
        return EXIT_FAILURE;
    }
    
    // create database
    mkdir(database_path, 0777);
    free(database_path);

    // add user database permission
    char* add_permission;
    if (strcmp(user_client, "root") == 0) add_permission = format_string(
        "echo '%s,%s' >> '%s'",
        database_name, user_client, permissions_path
    );
    else add_permission = format_string(
        "echo '%s,%s\n%s,root' >> '%s'",
        database_name, user_client, database_name, permissions_path
    );
    system(add_permission);
    free(add_permission);

    sprintf(response, "CREATE DATABASE OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _create_table(char* request, char* response) {
    // check database_used
    if (database_used == NULL) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used"
        );
        return EXIT_FAILURE;
    }

    // parsing request
    char table_name[BUFFER_SIZE];
    char constraints[BUFFER_SIZE];
    char* parse_str = "CREATE TABLE %s (%[^)]);";
    int parsed_items = sscanf(request, parse_str, table_name, constraints);

    if (parsed_items != 2) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "CREATE TABLE \e[3mtable_name\e[0m (\e[3mcolumn_name\e[0m \e[3mcolumn_type\e[0m, ...);"
        );
        return EXIT_FAILURE;
    }

    // check table
    char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) == 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table already exists"
        );
        return EXIT_FAILURE;
    }

    // create table
    FILE* table = fopen(table_path, "w");

    char* column_pair = strtok(constraints, ",");
    while (column_pair != NULL) {
        char column_name[BUFFER_SIZE];
        char column_type[BUFFER_SIZE];
        sscanf(column_pair, "%s %s", column_name, column_type);
        if (
            strcmp(column_type, "int") != 0 && 
            strcmp(column_type, "string") != 0
        ) {
            sprintf(
                response, "%s\n%s\n",
                "\e[91mError\e[39m: Invalid column type",
                "\e[3mcolumn_type\e[0m = { int, string }"
            );
            fclose(table);
            remove(table_path);
            return EXIT_FAILURE;
        }
        
        column_pair = strtok(NULL, ",");
        fprintf(table, "%s %s", column_name, column_type);
        if (column_pair != NULL) fprintf(table, ",");
    }
    fprintf(table, "\n");

    fclose(table);
    free(table_path);
    sprintf(response, "CREATE TABLE OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _drop_database(char* request, char* response) {
    // parse request
    char database_name[BUFFER_SIZE];

    char* parse_str = "DROP DATABASE %[^ ;];";
    int parsed_items = sscanf(request, parse_str, database_name);

    if (parsed_items != 1) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "DROP DATABASE \e[3mdatabase_name\e[0m;"
        );
        return EXIT_FAILURE;
    }

    // check permissions
    char* check_user_permission = format_string(
        "grep -wq '%s,%s' '%s'",
        database_name, user_client, permissions_path
    );

    if (system(check_user_permission) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User has no access to drop this database"
        );
        return EXIT_FAILURE;
    }
    free(check_user_permission);

    // check database
    char* database_path = format_string("%s/%s", databases_path, database_name);

    if (access(database_path, F_OK) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Database doesn't exist"
        );
        return EXIT_FAILURE;
    }

    // remove database
    char* remove_database = format_string("rm -rf '%s'", database_path);
    system(remove_database);
    free(remove_database);

    // revoke permission
    char* temp_permission_path = format_string("%s.tmp", permissions_path);

    char* revoke_permission = format_string(
        "grep -v '%s,' '%s' > '%s'",
        database_name, permissions_path, temp_permission_path
    );
    system(revoke_permission);
    free(revoke_permission);

    remove(permissions_path);
    rename(temp_permission_path, permissions_path);
    free(temp_permission_path);

    // change database used
    if (database_used != NULL) free(database_used);
    database_used = NULL;

    sprintf(response, "DROP DATABASE OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _drop_table(char* request, char* response) {
    if (!is_database_used()) {
        send_error_response(response, "\e[91mError\e[39m: No database used\n");
        return EXIT_FAILURE;
    }

    char table_name[BUFFER_SIZE];
    if (!parse_drop_table_request(request, table_name)) {
        send_error_response(response, "\e[91mError\e[39m: Invalid command\nDROP TABLE \e[3mtable_name\e[0m;\n");
        return EXIT_FAILURE;
    }

    if (!does_table_exist(table_name)) {
        send_error_response(response, "\e[91mError\e[39m: Table doesn't exist\n");
        return EXIT_FAILURE;
    }

    remove_table(table_name);
    send_response(response, "DROP TABLE OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int is_database_used() {
    return database_used != NULL;
}

int parse_drop_table_request(char* request, char* table_name) {
    char* parse_str = "DROP TABLE %[^ ;];";
    return sscanf(request, parse_str, table_name) == 1;
}

int does_table_exist(char* table_name) {
    char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);
    int result = access(table_path, F_OK) != 0;
    free(table_path);
    return !result;
}

void remove_table(char* table_name) {
    char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);
    char* remove_table = format_string("rm -rf '%s'", table_path);
    system(remove_table);
    free(remove_table);
    free(table_path);
}

void send_response(char *response, char *message) {
    sprintf(response, "%s", message);
}

void send_error_response(char *response, char *message) {
    sprintf(response, "%s", message);
}

int _drop_column(char* request, char* response) {
    if (database_used == NULL) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used"
        );
        return EXIT_FAILURE;
    }

    char column_name[BUFFER_SIZE];
    char table_name[BUFFER_SIZE];

    char* parse_str = "DROP COLUMN %s FROM %[^ ;];";
    int parsed_items = sscanf(request, parse_str, column_name, table_name);

    if (parsed_items != 2) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "DROP COLUMN \e[3mcolumn_name\e[0m FROM \e[3mtable_name\e[0m;"
        );
        return EXIT_FAILURE;
    }

    char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table doesn't exist"
        );
        return EXIT_FAILURE;
    }

    char* temp_table_path = format_string("%s.tmp", table_path);

    FILE* table = fopen(table_path, "r");
    FILE* temp_table = fopen(temp_table_path, "w");

    int column_index = 0;
    char row[BUFFER_SIZE];
    bool column_found = false;
    fscanf(table, " %[^\n]", row);

    char* column_pair = strtok(row, ",");
    while (column_pair != NULL) {
        char column[BUFFER_SIZE];
        sscanf(column_pair, "%s", column);

        if (strcmp(column, column_name) != 0) {
            if (column_index != 0) fprintf(temp_table, ",");
            fprintf(temp_table, "%s", column_pair);
        }
        else column_found = true;

        column_pair = strtok(NULL, ",");
        if (!column_found) column_index++;
    }
    fprintf(temp_table, "\n");

    if (!column_found) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Column doesn't exist"
        );
        fclose(table);
        fclose(temp_table);
        return EXIT_FAILURE;
    }

    // remove column
    while (fscanf(table, " %[^\n]", row) != EOF) {
        int index = 0;
        char* cell = strtok(row, ",");
        while (cell != NULL) {
            if (index != column_index) {
                if (index != 0) fprintf(temp_table, ",");
                fprintf(temp_table, "%s", cell);
            }
            
            cell = strtok(NULL, ",");
            index++;
        }
        fprintf(temp_table, "\n");
    }

    fclose(table);
    fclose(temp_table);

    // rename temp_table;
    remove(table_path);
    rename(temp_table_path, table_path);
    free(temp_table_path);
    free(table_path);

    sprintf(response, "DROP COLUMN OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _insert(char* request, char* response) {
    // check database_used
    if (database_used == NULL) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used"
        );
        return EXIT_FAILURE;
    }

    // parse request
    char table_name[BUFFER_SIZE];
    char row[BUFFER_SIZE];

    char* parse_str = "INSERT INTO %s (%[^)]);";
    int parsed_items = sscanf(request, parse_str, table_name, row);

    if (parsed_items != 2) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "INSERT INTO \e[3mtable_name\e[0m (\e[3mvalue_column_1\e[0m, \e[3mvalue_column_2\e[0m, ...);"
        );
        return EXIT_FAILURE;
    }

    // check table
    char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table doesn't exist"
        );
        return EXIT_FAILURE;
    }

    // parse row value
    int total_column = 0;
    char new_row[BUFFER_SIZE][BUFFER_SIZE];
    char* value = strtok(row, ",");
    while (value != NULL) {
        hapus_space_depan(value);
        hapus_space(value);

        strcpy(new_row[total_column++], value);
        value = strtok(NULL, ",");
    }

    // check data type
    FILE* table = fopen(table_path, "a+");

    int column_index = 0;
    char columns[BUFFER_SIZE];
    fscanf(table, " %[^\n]", columns);

    char* column_pair = strtok(columns, ",");
    while (column_pair != NULL) {
        char column_type[BUFFER_SIZE];
        sscanf(column_pair, " %*[^ ] %s", column_type);
        if (
            strcmp(column_type, "int") == 0 &&
            !is_int(new_row[column_index])
        ) {
            sprintf(
                response, "%s %s is not a valid int\n",
                "\e[91mError\e[39m:", new_row[column_index]
            );
            return EXIT_FAILURE;
        }
        else if (
            strcmp(column_type, "string") == 0 &&
            !is_string(new_row[column_index])
        ) {
            sprintf(
                response, "%s %s is not a valid string\n%s\n",
                "\e[91mError\e[39m:", new_row[column_index],
                "'\e[3mexample string\e[0m' is a valid string"
            );
            return EXIT_FAILURE;
        }

        column_pair = strtok(NULL, ",");
        column_index++;
    }

    // add row into table
    for (int i = 0; i < total_column; i++) {
        if (i != 0) fprintf(table, ",");
        fprintf(table, "%s", new_row[i]);
    }
    fprintf(table, "\n");

    fclose(table);
    free(table_path);

    sprintf(response, "INSERT OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _update(char* request, char* response) {
    // check database_used
    if (database_used == NULL) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used"
        );
        return EXIT_FAILURE;
    }

    // parse request
    char table_name[BUFFER_SIZE];
    char column_name[BUFFER_SIZE];
    char value[BUFFER_SIZE];
    char where_column[BUFFER_SIZE];
    char where_value[BUFFER_SIZE];
    bool has_where = false;

    char* parse_str = "UPDATE %s SET %[^=]= %[^;W]WHERE %[^=]= %[^;];";
    int parsed_items = sscanf(
        request, parse_str,
        table_name, column_name, value, where_column, where_value
    );

    if (parsed_items != 3 && parsed_items != 5) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "UPDATE \e[3mtable_name\e[0m SET \e[3mcolumn_name\e[0m=\e[3mvalue\e[0m [ WHERE \e[3mcolumn_name\e[0m=\e[3mvalue\e[0m ];"
        );
        return EXIT_FAILURE;
    }

    has_where = parsed_items == 5;

    // remove trailing space from value
    hapus_space(value);
    if (has_where) hapus_space(where_value);

    // check table
    char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table doesn't exist"
        );
        return EXIT_FAILURE;
    }

    // open table
    char* temp_table_path = format_string("%s.tmp", table_path);

    FILE* table = fopen(table_path, "r");
    FILE* temp_table = fopen(temp_table_path, "w");

    // check column
    int index = 0;
    int column_index = -1;
    int where_column_index = -1;
    char row[BUFFER_SIZE];
    fscanf(table, " %[^\n]", row);

    char* column_pair = strtok(row, ",");
    while (column_pair != NULL) {
        char column[BUFFER_SIZE];
        char column_type[BUFFER_SIZE];
        sscanf(column_pair, "%s %s", column, column_type);

        if (strcmp(column, column_name) == 0) {
            column_index = index;
            if (
                strcmp(column_type, "int") == 0 &&
                !is_int(value)
            ) {
                sprintf(
                    response, "%s %s is not a valid int\n",
                    "\e[91mError\e[39m:", value
                );
                fclose(temp_table);
                remove(temp_table_path);
                return EXIT_FAILURE;
            }
            else if (
                strcmp(column_type, "string") == 0 &&
                !is_string(value)
            ) {
                sprintf(
                    response, "%s %s is not a valid string\n%s\n",
                    "\e[91mError\e[39m:", value,
                    "'\e[3mexample string\e[0m' is a valid string"
                );
                fclose(temp_table);
                remove(temp_table_path);
                return EXIT_FAILURE;
            }
        }

        if (has_where && strcmp(column, where_column) == 0) {
            where_column_index = index;
            if (
                strcmp(column_type, "int") == 0 &&
                !is_int(where_value)
            ) {
                sprintf(
                    response, "%s %s is not a valid int\n",
                    "\e[91mError\e[39m:", where_value
                );
                fclose(temp_table);
                remove(temp_table_path);
                return EXIT_FAILURE;
            }
            else if (
                strcmp(column_type, "string") == 0 &&
                !is_string(where_value)
            ) {
                sprintf(
                    response, "%s %s is not a valid string\n%s\n",
                    "\e[91mError\e[39m:", where_value,
                    "'\e[3mexample string\e[0m' is a valid string"
                );
                fclose(temp_table);
                remove(temp_table_path);
                return EXIT_FAILURE;
            }
        }

        if (index != 0) fprintf(temp_table, ",");
        fprintf(temp_table, "%s", column_pair);
        column_pair = strtok(NULL, ",");
        index++;
    }
    fprintf(temp_table, "\n");

    if (column_index == -1) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Column doesn't exist"
        );
        fclose(table);
        fclose(temp_table);
        return EXIT_FAILURE;
    }

    if (has_where && where_column_index == -1) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Where column doesn't exist"
        );
        fclose(table);
        fclose(temp_table);
        return EXIT_FAILURE;
    }

    // update column
    while (fscanf(table, " %[^\n]", row) != EOF) {
        bool is_valid_row = !has_where;
        if (has_where) {
            index = 0;
            char row_copy[BUFFER_SIZE];
            strcpy(row_copy, row);
            char* cell = strtok(row_copy, ",");
            while (cell != NULL) {
                if (
                    index == where_column_index &&
                    strcmp(cell, where_value) == 0
                ) {
                    is_valid_row = true;
                    break;
                }
                cell = strtok(NULL, ",");
                index++;
            }
        }
        if (is_valid_row) {
            index = 0;
            char row_copy[BUFFER_SIZE];
            strcpy(row_copy, row);
            char* cell = strtok(row_copy, ",");
            while (cell != NULL) {
                if (index != 0) fprintf(temp_table, ",");
                fprintf(
                    temp_table, "%s",
                    index == column_index
                    ? value : cell
                );
                cell = strtok(NULL, ",");
                index++;
            }
        }
        else fprintf(temp_table, "%s", row);
        fprintf(temp_table, "\n");
    }

    fclose(table);
    fclose(temp_table);

    // rename temp_table;
    remove(table_path);
    rename(temp_table_path, table_path);
    free(temp_table_path);
    free(table_path);

    sprintf(response, "UPDATE OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _delete(char* request, char* response) {
    // check database_used
    if (database_used == NULL) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used"
        );
        return EXIT_FAILURE;
    }

    // parse request
    char table_name[BUFFER_SIZE];
    char where_column[BUFFER_SIZE];
    char where_value[BUFFER_SIZE];
    bool has_where = false;

    char* parse_str = "DELETE FROM %[^;W]WHERE %[^=]= %[^;];";
    int parsed_items = sscanf(
        request, parse_str,
        table_name, where_column, where_value
    );

    if (parsed_items != 1 && parsed_items != 3) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "DELETE FROM \e[3mtable_name\e[0m [ WHERE \e[3mcolumn_name\e[0m=\e[3mvalue\e[0m ];"
        );
        return EXIT_FAILURE;
    }

    has_where = parsed_items == 3;

    // remove trailling space from value
    hapus_space(table_name);
    if (has_where) hapus_space(where_value);

    // check table
    char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table doesn't exist"
        );
        return EXIT_FAILURE;
    }

    // open table
    char* temp_table_path = format_string("%s.tmp", table_path);

    FILE* table = fopen(table_path, "r");
    FILE* temp_table = fopen(temp_table_path, "w");

    // check column
    int index = 0;
    int where_column_index = -1;
    char row[BUFFER_SIZE];
    fscanf(table, " %[^\n]", row);

    char* column_pair = strtok(row, ",");
    while (column_pair != NULL) {
        char column_name[BUFFER_SIZE];
        char column_type[BUFFER_SIZE];
        sscanf(column_pair, "%s %s", column_name, column_type);

        if (has_where && strcmp(column_name, where_column) == 0) {
            where_column_index = index;
            if (
                strcmp(column_type, "int") == 0 &&
                !is_int(where_value)
            ) {
                sprintf(
                    response, "%s %s is not a valid int\n",
                    "\e[91mError\e[39m:", where_value
                );
                fclose(temp_table);
                remove(temp_table_path);
                return EXIT_FAILURE;
            }
            else if (
                strcmp(column_type, "string") == 0 &&
                !is_string(where_value)
            ) {
                sprintf(
                    response, "%s %s is not a valid string\n%s\n",
                    "\e[91mError\e[39m:", where_value,
                    "'\e[3mexample string\e[0m' is a valid string"
                );
                fclose(temp_table);
                remove(temp_table_path);
                return EXIT_FAILURE;
            }
        }

        if (index != 0) fprintf(temp_table, ",");
        fprintf(temp_table, "%s", column_pair);
        column_pair = strtok(NULL, ",");
        index++;
    }
    fprintf(temp_table, "\n");

    if (has_where && where_column_index == -1) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Where column doesn't exist"
        );
        return EXIT_FAILURE;
    }

    // delete row
    while (fscanf(table, " %[^\n]", row) != EOF) {
        bool is_valid_row = !has_where;
        if (has_where) {
            int index = 0;
            char row_copy[BUFFER_SIZE];
            strcpy(row_copy, row);
            char* cell = strtok(row_copy, ",");
            while (cell != NULL) {
                if (
                    index == where_column_index &&
                    strcmp(cell, where_value) == 0
                ) {
                    is_valid_row = true;
                    break;
                }
                cell = strtok(NULL, ",");
                index++;
            }
        }
        if (is_valid_row) continue;
        fprintf(temp_table, "%s\n", row);
    }

    fclose(table);
    fclose(temp_table);

    // rename temp_table;
    remove(table_path);
    rename(temp_table_path, table_path);
    free(temp_table_path);
    free(table_path);

    sprintf(response, "DELETE OK\n");
    pesan_log(request);
    return EXIT_SUCCESS;
}

int _select(char* request, char* response) {
    // check database_used
    if (database_used == NULL) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used"
        );
        return EXIT_FAILURE;
    }

    // parse request
    char select_columns[BUFFER_SIZE];
    char table_name[BUFFER_SIZE];
    char where_column[BUFFER_SIZE];
    char where_value[BUFFER_SIZE];
    bool select_all = false;
    bool has_where = false;

    char* parse_str = "SELECT %[^F]FROM %[^ ;]%*[; ] WHERE %[^=]= %[^;];";
    int parsed_items = sscanf(
        request, parse_str,
        select_columns, table_name, where_column, where_value
    );

    if (parsed_items != 2 && parsed_items != 4) {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "SELECT [ * | \e[3mcolumn_name\e[0m [, ...] ] FROM \e[3mtable_name\e[0m [ WHERE \e[3mcolumn_name\e[0m=\e[3mvalue\e[0m ];"
        );
        return EXIT_FAILURE;
    }

    has_where = parsed_items == 4;

    if (has_where) hapus_space(where_value);
    hapus_space(select_columns);

    // set select all
    select_all = strlen(select_columns) == 1 && select_columns[0] == '*';

    // check table
    char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) != 0) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table doesn't exist"
        );
        return EXIT_FAILURE;
    }

    // open table
    FILE* table = fopen(table_path, "r");

    // check columns
    int arg_column = 0;
    int total_column = 0;
    int column_id[BUFFER_SIZE];
    char columns[BUFFER_SIZE][BUFFER_SIZE];
    
    char row[BUFFER_SIZE];
    fscanf(table, " %[^\n]", row);

    strcpy(response, "");
    if (select_all) {
        strcat(response, row);
    }
    else {
        char* column_name = strtok(select_columns, ",");
        while (column_name != NULL) {
            hapus_space_depan(column_name);
            hapus_space(column_name);

            strcpy(columns[arg_column++], column_name);
            column_name = strtok(NULL, ",");
        }
        for (int i = 0; i < arg_column; i++) {
            int index = 0;
            char row_copy[BUFFER_SIZE];
            strcpy(row_copy, row);

            char* column_pair = strtok(row_copy, ",");
            while (column_pair != NULL) {
                char column[BUFFER_SIZE];
                sscanf(column_pair, "%s", column);
                if (strcmp(column, columns[i]) == 0)
                {
                    column_id[total_column++] = index;
                    if (i != 0) strcat(response, ",");
                    strcat(response, column_pair);
                    break;
                }
                column_pair = strtok(NULL, ",");
                index++;
            }
        }
    }
    strcat(response, "\n");

    int column_index = 0;
    int where_column_index = -1;
    char* column_pair = strtok(row, ",");
    if (has_where) {
        while (column_pair != NULL) {
            char column_name[BUFFER_SIZE];
            char column_type[BUFFER_SIZE];
            sscanf(column_pair, "%s %s", column_name, column_type);

            if (strcmp(column_name, where_column) == 0) {
                where_column_index = column_index;
                if (
                    strcmp(column_type, "int") == 0 &&
                    !is_int(where_value)
                ) {
                    sprintf(
                        response, "%s %s is not a valid int\n",
                        "\e[91mError\e[39m:", where_value
                    );
                    return EXIT_FAILURE;
                }
                else if (
                    strcmp(column_type, "string") == 0 &&
                    !is_string(where_value)
                ) {
                    sprintf(
                        response, "%s %s is not a valid string\n%s\n",
                        "\e[91mError\e[39m:", where_value,
                        "'\e[3mexample string\e[0m' is a valid string"
                    );
                    return EXIT_FAILURE;
                }
            }

            column_pair = strtok(NULL, ",");
            column_index++;
        }
    }

    if (!select_all && total_column != arg_column) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Select has invalid column/s"
        );
        return EXIT_FAILURE;
    }

    if (has_where && where_column_index == -1) {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Where column doesn't exist"
        );
        return EXIT_FAILURE;
    }

    // select corresponding cell
    while (fscanf(table, " %[^\n]", row) != EOF) {
        bool is_valid_row = !has_where;
        if (has_where) {
            int index = 0;
            char row_copy[BUFFER_SIZE];
            strcpy(row_copy, row);
            char* cell = strtok(row_copy, ",");
            while (cell != NULL) {
                if (
                    index == where_column_index &&
                    strcmp(cell, where_value) == 0
                ) {
                    is_valid_row = true;
                    break;
                }
                cell = strtok(NULL, ",");
                index++;
            }
        }
        if (is_valid_row) {
            if (select_all) strcat(response, row);
            else {
                for (int i = 0; i < total_column; i++) {
                    int index = 0;
                    char row_copy[BUFFER_SIZE];
                    strcpy(row_copy, row);
                    char* cell = strtok(row_copy, ",");
                    while (cell != NULL) {
                        if (index == column_id[i]) {
                            if (i != 0) strcat(response, ",");
                            strcat(response, cell);
                            break;
                        }
                        cell = strtok(NULL, ",");
                        index++;
                    }
                }
            }
            strcat(response, "\n");
        }
    }

    fclose(table);
    free(table_path);

    pesan_log(request);
    return EXIT_SUCCESS;
}

int _backup(char *request, char *response)
{
    char database[BUFFER_SIZE];

    char auth[BUFFER_SIZE];

    if (sscanf(request, "BACKUP %s %[^\n]", database, auth) == 2)
    {
        _auth_user(auth, response);
    }

    // check if database exists
    int databaseFound = 0;
    DIR *dir = opendir("databases");
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_DIR)
        {
            if (strcmp(entry->d_name, database) == 0)
            {
                databaseFound = 1;
                break;
            }
        }
    }

    if (!databaseFound)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Database does not exists");
        return EXIT_FAILURE;
    }

    char sendMessage[10000] = "";

    char* file_name = format_string("databases/log/%s.log", database);
    FILE *file = fopen(file_name, "r");

    if (file == NULL)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Failed to find log file");
        perror("fopen");
        return EXIT_FAILURE;
    }

    char line[BUFFER_SIZE];

    while (fgets(line, sizeof(line), file))
    {
        int colonCount = 0;
        char *ptr = line;

        while (*ptr != '\0')
        {
            if (*ptr == ':')
            {
                colonCount++;

                if (colonCount == 4)
                {
                    ptr++;
                    strcat(sendMessage, ptr);
                    break;
                }
            }

            ptr++;
        }
    }

    fclose(file);
    free(file_name);

    return EXIT_SUCCESS;
}

int query(char *request, char *response) {
    char user_name[BUFFER_SIZE], command[BUFFER_SIZE];
    sscanf(request, "%[^;];\n%[^;];", user_name, command);
    if (user_client != NULL) free(user_client);
    user_client = format_string("%s", user_name);

    if (strstr(command, "BACKUP")) {
        _backup(command, response);
    }
    else if (strstr(command, "AUTH USER")) {
        _auth_user(command, response);
    }
    else if (strstr(command, "CREATE USER")) {
        _create_user(command, response);
    }
    else if (strstr(command, "USE")) {
        _use_database(command, response);
    }
    else if (strstr(command, "GRANT PERMISSION")) {
        _grant_permission(command, response);
    }
    else if (strstr(command, "CREATE DATABASE")) {
        _create_database(command, response);
    }
    else if (strstr(command, "CREATE TABLE")) {
        _create_table(command, response);
    }
    else if (strstr(command, "DROP DATABASE")) {
        _drop_database(command, response);
    }
    else if (strstr(command, "DROP TABLE")) {
        _drop_table(command, response);
    }
    else if (strstr(command, "DROP COLUMN")) {
        _drop_column(command, response);
    }
    else if (strstr(command, "INSERT")) {
        _insert(command, response);
    }
    else if (strstr(command, "UPDATE")) {
        _update(command, response);
    }
    else if (strstr(command, "DELETE")) {
        _delete(command, response);
    }
    else if (strstr(command, "SELECT")) {
        _select(command, response);
    }
    else {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Unknown command"
        );
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int main(int argc, char const *argv[]) {
    char workDir[MAXDATALEN];
    getcwd(workDir, sizeof(workDir));

    pid_t pid, sid;       

    pid = fork();   

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
   
    if (pid > 0) {
        printf("PID : %d || %sn\n", pid, workDir);
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir(workDir)) < 0) {
        
        exit(EXIT_FAILURE);
    }

    server_db();
    printf("Server running on 127.0.0.1:%d\n", PORT);

    struct sockaddr_in address;
    int addrlen = sizeof(address);
    int server_fd, new_socket, valread, opt = 1;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    while (1)
    {
        char request[BUFFER_SIZE], response[BUFFER_SIZE];

        if (recv(new_socket, request, BUFFER_SIZE, 0) == 0)
        {
            if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            else
                recv(new_socket, request, BUFFER_SIZE, 0);
        }

        query(request, response);
        send(new_socket, response, BUFFER_SIZE, 0);
    }

    return 0;
}

