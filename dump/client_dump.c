#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 8080

struct permission
{
  char name[10000];
  char pass[10000];
};

int checkPermission(const char *uname, const char *pass)
{
  FILE *file = fopen("../database/databases/databaseusers/users", "r");

  if (file == NULL)
  {
    perror("Error opening the file");
    return 0;
  }

  char line[200];
  int mark = 0;

  while (fgets(line, sizeof(line), file))
  {
    char temp_uname[100], temp_pass[100];

    if (sscanf(line, "%99[^,], %99[^\n]", temp_uname, temp_pass) == 2)
    {
      if (strcmp(temp_uname, uname) == 0 && strcmp(temp_pass, pass) == 0)
      {
        mark = 1;
        break;
      }
    }
  }

  fclose(file);

  if (mark == 0)
  {
    printf("Not Allowed: No permission\n");
    return 0;
  }
  else
    return 1;
}

void copyToBackup(const char *logFileName, const char *backupFileName)
{
  FILE *logFile = fopen(logFileName, "r");
  FILE *backupFile = fopen(backupFileName, "w");

  if (logFile == NULL || backupFile == NULL)
  {
    perror("Error opening files");
    return;
  }

  char buffer[20000];
  while (fgets(buffer, sizeof(buffer), logFile))
  {
    char temp_buff[20000];
    snprintf(temp_buff, sizeof temp_buff, "%s", buffer);

    // Extracting action part after the last colon ':'
    char *lastColon = strrchr(temp_buff, ':');
    if (lastColon != NULL)
    {
      // Writing only the action part to the backup file
      fputs(lastColon + 1, backupFile);
    }
  }

  fclose(logFile);
  fclose(backupFile);
}


int main(int argc, char *argv[])
{
  if (argc < 6)
  {
    fprintf(stderr, "Usage: %s <arg1> <arg2> <arg3> <arg4> <arg5>\n", argv[0]);
    return 1;
  }

  int allowed = 0;

  if (geteuid() == 0)
    allowed = 1;
  else
    allowed = checkPermission(argv[2], argv[4]);

  if (allowed == 0)
    return 0;

  char logFileLoc[10000];
  snprintf(logFileLoc, sizeof logFileLoc, "../database/databases/log/%s.log", argv[5]);

  char backupFileLoc[10000];
  snprintf(backupFileLoc, sizeof backupFileLoc, "%s.backup", argv[5]);

  // Copy the contents of the log file to the backup file
  copyToBackup(logFileLoc, backupFileLoc);

  // Use stdin (file descriptor 0) for reading commands from the redirected file
  FILE *file = stdin;

  if (file == NULL)
  {
    perror("Error opening the input file");
    return 0;
  }

  char buff[20000];
  int mark = 0;

  while (fgets(buff, sizeof(buff), file))
  {
    char *token;
    char temp_buff[20000];
    snprintf(temp_buff, sizeof temp_buff, "%s", buff);
    token = strtok(temp_buff, ":");

    int i = 0;
    char cmd[100][10000];

    while (token != NULL && i < 100)
    {
      strcpy(cmd[i], token);
      i++;
      token = strtok(NULL, ":");
    }

    if (i >= 5)
    {
      char *b = strstr(cmd[4], "USE");
      if (b != NULL)
      {
        char *tokens;
        char temp_cmd[20000];
        strcpy(temp_cmd, cmd[4]);
        tokens = strtok(temp_cmd, "; ");

        int j = 0;
        char use_cmd[100][10000];

        while (tokens != NULL && j < 100)
        {
          strcpy(use_cmd[j], tokens);
          j++;
          tokens = strtok(NULL, "; ");
        }

        if (j >= 2 && strcmp(use_cmd[1], argv[5]) == 0)
          mark = 1;
        else
          mark = 0;
      }
    }

    if (mark == 1)
      printf("%s", buff);
  }

  fclose(file);

  return 0;
}

